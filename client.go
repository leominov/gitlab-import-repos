package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
)

type Client struct {
	config *Config
	c      *http.Client
}

type ImportRepoRequest struct {
	Name        string `json:"name"`
	NamespaceID int    `json:"namespace_id"`
	ImportURL   string `json:"import_url"`
}

func NewClient(config *Config) *Client {
	httpClient := http.DefaultClient
	httpClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	return &Client{
		config: config,
		c:      httpClient,
	}
}

func (c *Client) GetAPIPath(path string) string {
	return fmt.Sprintf("%s/api/v4%s?private_token=%s", c.config.GitLabUrl, path, c.config.GitLabToken)
}

func (c *Client) GetWebPath(path string) string {
	return fmt.Sprintf("%s%s?private_token=%s", c.config.GitLabUrl, path, c.config.GitLabToken)
}

func (c *Client) doRequest(method, path string, body interface{}) (*http.Response, error) {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, c.GetAPIPath(path), buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Close = true
	return c.c.Do(req)
}

func getNameFromUrl(url string) string {
	nameRaw := path.Base(url)
	name := strings.TrimSuffix(nameRaw, ".git")
	return name
}

func (c *Client) ImportRepo(u *url.URL) error {
	name := getNameFromUrl(u.String())
	u.User = url.UserPassword(u.User.Username(), c.config.StashToken)
	body := &ImportRepoRequest{
		Name:        name,
		NamespaceID: c.config.NamespaceID,
		ImportURL:   u.String(),
	}
	logrus.WithField("body", fmt.Sprintf("%v", body)).Info("Importing...")
	if c.config.DryRun {
		return nil
	}
	resp, err := c.doRequest("POST", "/projects", body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusCreated {
		return nil
	}
	return fmt.Errorf("incorrect response code: %s", resp.Status)
}

func (c *Client) ImportRepos(repos []string) error {
	for _, u := range repos {
		repoUrl, err := url.Parse(u)
		if err != nil {
			return err
		}
		err = c.ImportRepo(repoUrl)
		if err != nil {
			if c.config.StopOnError {
				return fmt.Errorf("error import %s: %v", u, err)
			}
			logrus.WithField("url", u).Errorf("Error import: %v", err)
			continue
		}
		logrus.WithField("url", u).Info("import: ok")
	}
	return nil
}
