package main

import (
	"flag"

	"github.com/sirupsen/logrus"
)

var (
	configFileFlag = flag.String("config.file", "config.yml", "Configuration file")
)

func main() {
	flag.Parse()
	c, err := ConfigFromFile(*configFileFlag)
	if err != nil {
		logrus.Fatal(err)
	}
	cli := NewClient(c)
	err = cli.ImportRepos(c.Repos)
	if err != nil {
		logrus.Fatal(err)
	}
}
