package main

import (
	"io/ioutil"

	"strings"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	GitLabUrl   string   `yaml:"gitlab_url"`
	GitLabToken string   `yaml:"gitlab_private_token"`
	StashToken  string   `yaml:"stash_access_token"`
	StopOnError bool     `yaml:"stop_on_error"`
	DryRun      bool     `yaml:"dry_run"`
	Repos       []string `yaml:"repos"`
	NamespaceID int      `yaml:"namespace_id"`
}

func ConfigFromFile(file string) (*Config, error) {
	configBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	var c *Config
	if err := yaml.Unmarshal(configBytes, &c); err != nil {
		return nil, err
	}
	c.GitLabUrl = strings.TrimSuffix(c.GitLabUrl, "/")
	return c, nil
}
