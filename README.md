# GitLab import repos

## Configuration

```
---
stash_access_token: ABC
gitlab_private_token: ABC
gitlab_url: https://gitlab.local/
namespace_id: 1
stop_on_error: false
dry_run: false
repos:
  - https://username@stash.local/scm/group/project.git
```
